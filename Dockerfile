FROM node:10-alpine3.10

RUN apk --no-cache add \
    jq=1.6-r0 \
    bash=5.0.0-r0

RUN wget -P / https://bitbucket.org/bitbucketpipelines/bitbucket-pipes-toolkit-bash/raw/0.4.0/common.sh

COPY pipe /
COPY LICENSE.txt README.md pipe.yml /

ENTRYPOINT ["/pipe.sh"]
